import { Component } from '@angular/core';
import { changefill } from '../assets/js/changefill.js'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'task1';
    cart = true;

    texts: Array<any> = [
        { text: 'Premium Bestseller'},
        { text: 'Economy Bestseller'}
    ];
    
    list: Array<any> = [
        { status: 'available'},
        { status: 'limited', text: '8 Tires Left'},
        { status: 'na', text: 'Back in 1 week', class:'textdn'},
    ];

    menulist: Array<any> = [
        {text: 'Account', id:'account'},
        {text: 'Orders', id:'orders'},
        {text: 'Cart', id:'cart'}
    ];

    ngOnInit() {
        setTimeout(function() {
            var a = document.querySelectorAll('.btn-cart');
            for (var i = 0; i < a.length; i++) {
                changefill.set(a[i], 'black');
            }

            var color = getComputedStyle(document.body).getPropertyValue('--maroon');
            changefill.set(document.getElementById('r-cross-svg'), color);
        }, 0);
    }

    private inc(t){
        var value = parseInt(t.target.parentNode.children[1].value);
        value += parseInt(t.target.value);
        if(value >= 0 && value < 100) {
            t.target.parentNode.children[1].value = value;
        }
    }
}
