
var a = {
    set : function (elem, color) {
        elem.addEventListener("load", function(e) {
            var path = e.path[0].contentDocument.getElementsByTagName('path');
            var ellipse = e.path[0].contentDocument.getElementsByTagName('ellipse');
            var rect = e.path[0].contentDocument.getElementsByTagName('rect');
            var line = e.path[0].contentDocument.getElementsByTagName('line');
            
            if(path.length != 0) {
                for(var i=0; i<path.length; i++) {
                    path[i].setAttribute("fill", color);
                }
            }
            if(ellipse.length != 0) {
                for(var i=0; i<ellipse.length; i++) {
                    ellipse[i].setAttribute("fill", color);
                }
            }
            if(rect.length != 0) {
                for(var i = 0; i < rect.length; i++) {
                    rect[i].setAttribute("fill", color);
                }
            }
            if (line.length != 0) {
                for (var i = 0; i < line.length; i++) {
                    line[i].setAttribute("fill", color);
                }
            }
        }, false);   
    }
}
var changefill = a;
export { changefill};
